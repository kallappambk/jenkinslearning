/**********************************************************************************
**
** Copyright (C) 2019 Verolt Engineering pvt. ltd.
** Contact: https://www.verolt.com
**
************************************************************************************/

#include "mqttclient_helperclass.h"

bool MqttClient_HelperClass::m_bIsMqttInstanceExists;
MqttClient_HelperClass* MqttClient_HelperClass::m_pMqttClientHelperInstance;

MqttClient_HelperClass::MqttClient_HelperClass(QObject *parent) : QObject(parent)
{
    m_pMqttClient = new QMqttClient(this);
}

MqttClient_HelperClass::~MqttClient_HelperClass()
{
    m_bIsMqttInstanceExists = false;
}

MqttClient_HelperClass *MqttClient_HelperClass::getInstance()
{
    if(!m_bIsMqttInstanceExists){
        m_pMqttClientHelperInstance = new MqttClient_HelperClass();
        m_bIsMqttInstanceExists = true;
        return m_pMqttClientHelperInstance;
    }
    else
    {
        return m_pMqttClientHelperInstance;
    }
}

QObject *MqttClient_HelperClass::pMqttSingletonInstance(QQmlEngine *qEngine, QJSEngine *qJavaScriptEngine)
{
    Q_UNUSED(qEngine)
    Q_UNUSED(qJavaScriptEngine)
    return MqttClient_HelperClass::getInstance();
}

void MqttClient_HelperClass::i_vInitializeClient(const QString &qstrHostName, quint16 qu16PortNo)
{
    m_pMqttClient->setHostname(qstrHostName);
    m_pMqttClient->setPort(qu16PortNo);
    m_pMqttClient->connectToHost();

    connect(m_pMqttClient, &QMqttClient::connected, this, &MqttClient_HelperClass::sl_vBrokerConnected);
    connect(m_pMqttClient, &QMqttClient::messageReceived,this, &MqttClient_HelperClass::sl_vCheckTopicType);
    connect(m_pMqttClient, &QMqttClient::disconnected, this, &MqttClient_HelperClass::sl_vBrokerDisconnected);
}

int MqttClient_HelperClass::i_s32MessagePublish(const QString &qstrTopic, const QString &qstrMessage,unsigned char qos, bool retain)      //To publish the message over MQTT
{
    int s32Result = -1;
    s32Result = m_pMqttClient->publish(QMqttTopicName(qstrTopic),qstrMessage.toUtf8(),qos,retain);

    //TODO
    //On publish failure, add define for success/error
    return s32Result;     //Returns 0 on success
}

void MqttClient_HelperClass::sl_vBrokerConnected()
{
    qDebug() << Q_FUNC_INFO << " : Client is Connected to broker." << m_pMqttClient->clientId();
    for (int s32Index = 0 ; s32Index < m_qslTopicsList.count() ; s32Index++ )
    {
        if (s32Index < MAX_MQTT_TOPICS)
        {

            if(!m_qslTopicsList.at(s32Index).isNull()){
                qDebug() << "SUNBSCRIBED TO TOPIC" << m_qslTopicsList.at(s32Index);
                m_pMqttClient->subscribe(m_qslTopicsList.at(s32Index)); //on success returns pointer to QmqttSubscriber
            }
        }
    }
}

void MqttClient_HelperClass::sl_vBrokerDisconnected()
{
    qDebug() << Q_FUNC_INFO << " : Client is Disconnected from broker.";
    //TODO
    //On disconnected, add disconnected notification
}

void MqttClient_HelperClass::sl_vCheckTopicType(const QByteArray &qbaMessage, const QMqttTopicName &qMqttTopic)
{
    qDebug() << qbaMessage;
    if (qMqttTopic.name().compare(m_qslTopicsList.at(0)) == 0)
    {
        emit si_vOnEmbeddedTopicReceived(qbaMessage);
    }
    else if (qMqttTopic.name().compare(m_qslTopicsList.at(1)) == 0)
    {
        emit si_vOnWebGLTopicReceived(qbaMessage,qMqttTopic);
    }
    //TODO
    //Add checks for more registered topics and emit respective signals
}

void MqttClient_HelperClass::i_vRegisterTopics(const QString& qstrTopic)
{
    m_qslTopicsList.append(qstrTopic);
}
