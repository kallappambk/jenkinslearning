import QtQuick 2.14
import QtQuick.Window 2.14
import MQTT 1.0

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    onWindowStateChanged: {
        kk.i_vInitializeClient("broker.hivemq.com",1883);
        kk.i_vRegisterTopics("TEST_MQTT");
    }

    MQTT {
        id : kk
    }

    Text {
        id: txt
        text: qsTr("CLICK HERE")
        anchors.centerIn: parent
        height: 200
        width: 200

    }
    MouseArea {
        id : ma_txt
        anchors.fill: parent
        onClicked:{
	 	kk.i_s32MessagePublish("TEST_MQTT","TEST_MESSAGE",2,false);
//Added comment for check	
}
    }
}
