/**********************************************************************************
**
** Copyright (C) 2019 Verolt Engineering pvt. ltd.
** Contact: https://www.verolt.com
**
************************************************************************************/

#ifndef MQTTCLIENT_HELPERCLASS_H
#define MQTTCLIENT_HELPERCLASS_H

#include <QMqttClient>
#include <QQmlEngine>
#include <QDebug>

#define MAX_MQTT_TOPICS 10
#define DEFAULT_PUBLISH_STATE -1


class MqttClient_HelperClass: public QObject
{
    Q_OBJECT

public:
    explicit MqttClient_HelperClass(QObject *parent = nullptr);
    ~MqttClient_HelperClass();

    /**
     * @brief getInstance
     * @return MqttClient_HelperClass instance
     * @details Returns singleton instance of MqttClient_HelperClass
     */
    static MqttClient_HelperClass* getInstance();

    //copy constructor
    MqttClient_HelperClass(const MqttClient_HelperClass&);

    MqttClient_HelperClass& operator=(const MqttClient_HelperClass&);

    /**
     * @brief pMqttSingletonInstance
     * @param qEngine
     * @param qJavaScriptEngine
     * @return MqttClientHelperClass singleton instance
     * @details Singleton instance provider to QML.
        */
    static QObject *pMqttSingletonInstance(QQmlEngine *qEngine, QJSEngine *qJavaScriptEngine);

    /**
      * @brief i_s32MessagePublish
      * @param qstrTopic
      * @param qstrMessage
      * @param qos
      * @param retain
      * @return result
      * @details Publishes message to the broker with topic
      */
    Q_INVOKABLE int i_s32MessagePublish(const QString &qstrTopic, const QString &qstrMessage,unsigned char qos = 2, bool retain = false);

    /**
      * @brief i_vInitializeClient
      * @details Init connection to Mqtt Broker with Portnumber and Hostname
      * @details Connects Mqtt signals to handler slots
      */
    Q_INVOKABLE void i_vInitializeClient(const QString &qstrHostName, quint16 qu16PortNo);

    /**
      * @brief i_vRegisterTopics
      * @param qstrTopic
      * @details Registers and updates new incoming qstrTopic to Topic list
      */
    Q_INVOKABLE void i_vRegisterTopics(const QString &qstrTopic);

private slots:

    /**
     * @brief sl_vBrokerConnected
     * @details On connect to the MQTT broker, checks for Maximum number of topics that can be added
     * Adds a new subscription from m_qslTopicsList to receive notifications on topic
     */
    void sl_vBrokerConnected();

    /**
     * @brief sl_vBrokerDisconnected
     * @details On disconnect from the MQTT broker this slot is triggered
     * @todo Add action to notify disconnected signal
     */
    void sl_vBrokerDisconnected();

    /**
     * @brief sl_vCheckTopicType
     * @param qbaMessage
     * @param qMqttTopic
     * @details On Mqtt message received, checks for topic name
     * @details Emits signal to Mealplan if topic name is Embedded
     */
    void sl_vCheckTopicType(const QByteArray &qbaMessage, const QMqttTopicName &qMqttTopic);

signals:

    /**
     * @brief si_vOnEmbeddedTopicReceived
     * @param qbaMessages
     * @details On Mqtt message received, notifies Mealplan
     */
    void si_vOnEmbeddedTopicReceived(const QByteArray &qbaMessage);

    /**
     * @brief si_vOnWebGLTopicReceived
     * @param qbaMessage
     * @param qMqttTopic
     * @details On Mqtt message received, handles for WebGL
     */
    void si_vOnWebGLTopicReceived(const QByteArray &qbaMessage,const QMqttTopicName &qMqttTopic);

public:
    static bool m_bIsMqttInstanceExists;
    static MqttClient_HelperClass* m_pMqttClientHelperInstance;
    QMqttClient *m_pMqttClient;
    QStringList m_qslTopicsList;
};

#endif
