import QtQuick 2.14
import QtQuick.Window 2.14
import "Components"

Window {
    id: window
    width: 640
    height: 480
    color: "#03002e"
    visible: true
    title: qsTr("SpeedoMeter")

    FontLoader { id: textFont ; source: "../Fonts/CenturyGothicRegular.ttf"}

    Arcguage {
        id: comp_Arcs
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width<parent.height?parent.width:parent.height
        height: width
    }
}
