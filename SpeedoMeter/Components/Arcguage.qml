import QtQuick 2.14
import QtQuick.Controls 2.14
import QtGraphicalEffects 1.14
import QtQuick.Shapes 1.11


Item {
    id: item_speedMeter
    property int pr_s32Size: width

    Image {
        id: img_Arcs
        width: pr_s32Size
        height: pr_s32Size
        anchors.horizontalCenter: parent.horizontalCenter
        source: "../ASSETS/iconArcs.png"

        MeterMarks {
            id : comp_TextMarks
            anchors.fill: img_Arcs
        }
    }

    Shape {
        id : shaper_Needle
        height: 0.5 * pr_s32Size
        width: 0.06 * pr_s32Size
        z : 1
        anchors.verticalCenterOffset: -0.026* pr_s32Size
        anchors.horizontalCenterOffset: 0.0022 * pr_s32Size
        anchors.verticalCenter: img_Arcs.verticalCenter
        anchors.horizontalCenter: img_Arcs.horizontalCenter
        transform: Rotation { id: rot_Guage ; origin.x : shaper_Needle.width/2; origin.y : shaper_Needle.height * 0.9; angle: -108.4 }
        NumberAnimation { id : numANim_MeterGuage1; target: rot_Guage; property: "angle"; to: 108.3; duration: 8000 * 1.5; running: false;
            onFinished: numANim_MeterGuage2.running = true }

        NumberAnimation { id : numANim_MeterGuage2 ; target: rot_Guage ; property: "angle"; to:  -108.3 ; duration: 1000 ; running: false
            onFinished: numANim_MeterGuage1.running = true }

        ShapePath {
            strokeColor: "#FF0000"
            fillColor: "#FF0000"
            joinStyle: ShapePath.MiterJoin
            capStyle : ShapePath.RoundCap
            strokeStyle: ShapePath.SolidLine
            startX: shaper_Needle.width * 0.5; startY: shaper_Needle.height * 0.1
            PathLine { x: shaper_Needle.width * 0.5; y: shaper_Needle.height * 0.1  }
            PathLine { x: shaper_Needle.width * 0.9; y: shaper_Needle.height * 0.9 }
            PathLine { x: shaper_Needle.width * 0.1;  y: shaper_Needle.height * 0.9 }
        }

        Shape {
            id : shaper_Triangle
            width: 0.072 * pr_s32Size
            height: 0.045 * pr_s32Size
            anchors.horizontalCenter: shaper_Needle.horizontalCenter
            anchors.bottomMargin: -shaper_Needle.height * 0.05
            anchors.bottom: shaper_Needle.top

            ShapePath {
                strokeColor: "#DC0005"
                fillColor: "#DC0005"
                joinStyle: ShapePath.MiterJoin
                capStyle : ShapePath.RoundCap
                startX: shaper_Triangle.width * 0.5 ; startY: shaper_Triangle.height * 0.9
                PathLine { x: shaper_Triangle.width * 0.5; y: shaper_Triangle.height * 0.1 }
                PathLine { x: shaper_Triangle.width * 0.1; y: shaper_Triangle.height * 0.9 }
                PathLine { x: shaper_Triangle.width * 0.5; y: shaper_Triangle.height * 0.9 }
            }

            ShapePath {
                strokeColor: "#FF0000"
                fillColor: "#FF0000"
                joinStyle: ShapePath.MiterJoin
                capStyle : ShapePath.RoundCap
                startX: shaper_Triangle.width * 0.5 ; startY: shaper_Triangle.height * 0.9
                PathLine { x: shaper_Triangle.width * 0.5; y: shaper_Triangle.height * 0.1 }
                PathLine { x: shaper_Triangle.width * 0.9; y: shaper_Triangle.height * 0.9 }
                PathLine { x: shaper_Triangle.width * 0.5; y: shaper_Triangle.height * 0.9 }
            }
        }
    }

    Rectangle {
        id : rect_CenterCircle
        width: shaper_Needle.width * 1.5
        height: width
        radius: width
        z : 1
        color: "#808080"
        anchors.bottom: shaper_Needle.bottom
        anchors.horizontalCenter: shaper_Needle.horizontalCenter
    }

    Item {
        id: item_guageMark
        y: 0.2541*pr_s32Size
        width: 0.833*pr_s32Size
        height: width
        anchors.horizontalCenterOffset: 0.0020*pr_s32Size
        anchors.horizontalCenter: item_speedMeter.horizontalCenter

        Rectangle {
            id: rect_AnimRight
            width: 0.06*pr_s32Size
            height: 0.97916 * pr_s32Size
            radius: height/2
            anchors.centerIn: item_guageMark
            gradient: Gradient {
                GradientStop { position: 0.01; color: "#FFFFFF" }
                GradientStop { position: 0.02; color: "#00000000"}
            }
        }

        Rectangle {
            id: rect_AnimLeft
            width: 0.06 * pr_s32Size
            height: 0.97916 * pr_s32Size
            radius: height/2
            anchors.centerIn: item_guageMark

            gradient: Gradient {
                GradientStop { position: 0.01; color: "#FFFFFF" }
                GradientStop { position: 0.02; color: "#00000000"}
            }
        }
    }

    Metertext {
        id : comp_MeterText
        width : 0.21 * pr_s32Size
        height: width
        z : 0
        anchors.horizontalCenter: img_Arcs.horizontalCenter
        anchors.verticalCenter: img_Arcs.verticalCenter
        anchors.verticalCenterOffset : img_Arcs.height/3.25
        pr_s32AngleValue: (rot_Guage.angle < 0)? 50+(rot_Guage.angle/2.16) : 50+(rot_Guage.angle/2.16);
        NumberAnimation { target: comp_MeterText; property: "anchors.verticalCenterOffset"; from :-img_Arcs.height/4; to: img_Arcs.height/3.25; duration: 1000; running: true }
        NumberAnimation { target: comp_MeterText; property: "opacity"; from : 0; to : 1; duration: 1000; running: true }
    }

    ParallelAnimation {
        id : parAnim_StartAnim
        running: true
        RotationAnimation { target: rect_AnimLeft; to: 254; duration: 1500; direction: RotationAnimation.Counterclockwise }
        RotationAnimation { target: rect_AnimRight; to: 106; duration: 1500; direction: RotationAnimation.Clockwise }
        onStopped: {
            numANim_MeterGuage1.running = true
            rect_AnimRight.visible = rect_AnimLeft.visible = false
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:2;anchors_y:37}
}
##^##*/

