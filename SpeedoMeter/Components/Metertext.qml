import QtQuick 2.0
import QtGraphicalEffects 1.14

Rectangle {
    id: rect_NumberMeter
    color: "#00000000"

    property int pr_s32AngleValue
    Text {
        id: txt_NumberMeter
        width: rect_NumberMeter.width
        height: rect_NumberMeter.height
        color: "#FFFFFF"
        text: pr_s32AngleValue
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        anchors.centerIn: rect_NumberMeter
        font {family: textFont.name; pointSize: 0.30*rect_NumberMeter.width ; weight: Font.Bold; }
    }

    Text {
        id: txt_measure
        width: rect_NumberMeter.width
        height: rect_NumberMeter.height
        color: "#4A556A"
        text: "Kmph"
        anchors.verticalCenterOffset: rect_NumberMeter.width/2.25
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        anchors.centerIn: rect_NumberMeter
        font {family: textFont.name; pointSize: 0.125*rect_NumberMeter.width; weight: Font.Bold}
    }

    RectangularGlow {
        id: glow_RectNumberMeter
        opacity: 0.75
        anchors.fill: rect_NumberMeter
        glowRadius: rect_NumberMeter.width * 2
        cornerRadius: rect_NumberMeter.radius + glowRadius/1.5
        color: "#FFFFFF"
    }
}
