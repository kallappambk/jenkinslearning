import QtQuick 2.0

Item {
    id: item_TxtMarks
    property bool pr_bIndicatorLeftIsOn: false
    property bool pr_bIndicatorRightIsOn: false

    Text {
        id: txt_Mark25
        width: 0.05416 * item_TxtMarks.width
        height: width
        color: "#ffffff"
        text: qsTr("25")
        anchors.left: item_TxtMarks.left ; anchors.leftMargin: 0.025 * item_TxtMarks.width
        anchors.verticalCenter: item_TxtMarks.verticalCenter ; anchors.verticalCenterOffset: -0.15625 * item_TxtMarks.width
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font {family: textFont.name; pointSize: 0.029 * item_TxtMarks.width; weight: Font.Bold}
    }
    Text {
        id: txt_Mark50
        width: 0.05416 * item_TxtMarks.width
        height: width
        color: "#FFFFFF"
        text: qsTr("50")
        anchors.horizontalCenter: item_TxtMarks.horizontalCenter
        anchors.top: item_TxtMarks.top
        anchors.topMargin: 0.105 * item_TxtMarks.width
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font {family: textFont.name; pointSize: 0.029 * item_TxtMarks.width; weight: Font.Bold}
    }
    Text {
        id: txt_Mark75
        width: 0.05416 * item_TxtMarks.width
        height: width
        color: "#FFFFFF"
        text: qsTr("75")
        anchors.right: item_TxtMarks.right ; anchors.rightMargin: 0.025 * item_TxtMarks.width
        anchors.verticalCenter: item_TxtMarks.verticalCenter ; anchors.verticalCenterOffset: -0.15625 * item_TxtMarks.width
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font {family: textFont.name; pointSize: 0.029 * item_TxtMarks.width; weight: Font.Bold}
    }

    Image {
        id: img_LeftIndicator
        source: pr_bIndicatorLeftIsOn?"qrc:/ASSETS/img_LeftSig.png":"qrc:/ASSETS/img_LeftDis.png";
        width: 0.2/1.7 * item_TxtMarks.width
        height: 0.1/1.65 * item_TxtMarks.width
        anchors { left: item_TxtMarks.left; leftMargin: 0.190 * item_TxtMarks.width ; top: item_TxtMarks.top; topMargin: 0.105 * item_TxtMarks.width }
    }
    Image {
        id: img_RightIndicator
        source: pr_bIndicatorRightIsOn?"qrc:/ASSETS/img_RightSig.png":"qrc:/ASSETS/img_RghtDis.png";
        width: 0.2/1.7 * item_TxtMarks.width
        height: 0.1/1.65 * item_TxtMarks.width
        anchors { right: item_TxtMarks.right; rightMargin: 0.190 * item_TxtMarks.width ; top: item_TxtMarks.top; topMargin: 0.105 * item_TxtMarks.width }
    }
}
